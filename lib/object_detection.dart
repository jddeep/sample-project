import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:mlkit/mlkit.dart';

class ObjectDetectionPage extends StatefulWidget {
  @override
  _ObjectDetectionPageState createState() => new _ObjectDetectionPageState();
}

class _ObjectDetectionPageState extends State<ObjectDetectionPage> {
  File _file;

  List<VisionLabel> _currentLabels = <VisionLabel>[];

  bool _isMonumentImgCaptured = false;

  FirebaseVisionLabelDetector detector = FirebaseVisionLabelDetector.instance;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(primaryColor: Colors.amber,
       fontFamily: GoogleFonts.averageSans().fontFamily),
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Monument Detector', style: TextStyle(color: Colors.white)),
         actions: <Widget>[
           _isMonumentImgCaptured?IconButton(icon: Icon(Icons.refresh),
           color: Colors.white,
            onPressed: (){
              setState(() {
                _isMonumentImgCaptured = false;
                _currentLabels = <VisionLabel>[];
                _file = File('/');
              });
            }):Container(height:0.0)
         ],
         leading: IconButton(icon: Icon(Icons.arrow_back),
         color: Colors.white,
         onPressed: ()=>Navigator.pop(context),),
        ),
        body: Stack(
          children: <Widget>[
            _buildBody(_file),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: _isMonumentImgCaptured?Container(height:0.0):Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FloatingActionButton(
                      heroTag: 'fab-gallery',
                      backgroundColor: Colors.amber,
          onPressed: () async {
              try {
                var file =
                    await ImagePicker.pickImage(source: ImageSource.gallery);
                setState(() {
                  _file = file;
                });

                var currentLabels =
                    await detector.detectFromBinary(_file?.readAsBytesSync());
                setState(() {
                  _currentLabels = currentLabels;
                  if(_currentLabels != null && _currentLabels.length != 0)
                  _isMonumentImgCaptured = true;
                });
              } catch (e) {
                print(e.toString());
              }
          },
          child: new Icon(Icons.photo, color: Colors.white,),
        ),
        FloatingActionButton(
          backgroundColor: Colors.amber,
          onPressed: () async {
              try {
                var file =
                    await ImagePicker.pickImage(source: ImageSource.camera);
                setState(() {
                  _file = file;
                });

                var currentLabels =
                    await detector.detectFromBinary(_file?.readAsBytesSync());
                setState(() {
                  _currentLabels = currentLabels;
                  if(_currentLabels != null && _currentLabels.length != 0)
                  _isMonumentImgCaptured = true;
                });
              } catch (e) {
                print(e.toString());
              }
          },
          child: new Icon(Icons.camera, color: Colors.white,),
        ),
                  ],
                ),
              ),
            )
          ],
        ),
        // floatingActionButton: new FloatingActionButton(
        //   onPressed: () async {
        //     try {
        //       var file =
        //           await ImagePicker.pickImage(source: ImageSource.gallery);
        //       setState(() {
        //         _file = file;
        //       });

        //       var currentLabels =
        //           await detector.detectFromBinary(_file?.readAsBytesSync());
        //       setState(() {
        //         _currentLabels = currentLabels;
        //       });
        //     } catch (e) {
        //       print(e.toString());
        //     }
        //   },
        //   child: new Icon(Icons.photo),
        // ),
      ),
    );
  }

  //Build body
  Widget _buildBody(File _file) {
    return new Container(
      child: new Column(
        children: <Widget>[displaySelectedFile(_file), _buildList(_currentLabels)],
      ),
    );
  }

  Widget _buildList(List<VisionLabel> labels) {
    if (labels == null || labels.length == 0 ) {
      return Center(
        child: Column(
          children: <Widget>[
            Text('Hold your device camera as close as possible towards the monument OR Choose a photo from your gallery.',
            style: TextStyle(color: Colors.amber, fontFamily: GoogleFonts.averageSans().fontFamily, fontSize: 24.0),
            textAlign: TextAlign.center,
            ),
            !_isMonumentImgCaptured?Container(
              height: 175.0,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
              decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/monuments.jpeg'), fit: BoxFit.cover)),
            ):SizedBox.shrink()
          ],
        ),
      );
    }
    return new Expanded(
      child: new Container(    
        child: new ListView.builder(
            padding: const EdgeInsets.all(1.0),
            itemCount: labels.length,
            itemBuilder: (context, i) {
              return _buildRow(labels[i].label, labels[i].confidence.toString(), labels[i].entityID);
            }),
      ),
    );
  }

   Widget displaySelectedFile(File file) {
    return new SizedBox(
      // height: 200.0,
      width: 200.0,
      child: file == null
          ? new Text('')
          : new Image.file(file, fit: BoxFit.cover,),
    );
}

  //Display labels
  Widget _buildRow(String label, String confidence, String entityID) {
    return new ListTile(
      title: new Text(
        "\nLabel: $label \nConfidence: $confidence",
      ),
      dense: true,
    );
  }
}
