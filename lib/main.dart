import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:monumento/login_screen.dart';
import 'package:monumento/object_detection.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Monumento',
      initialRoute: '/',
      routes: {
        '/' : (context) => WelcomePage(),
        '/object-detection' : (context) => ObjectDetectionPage(),
        '/login-screen' : (context) => LoginScreen(),
      },
    );
  }
}

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child:Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/monuments.jpeg'))),
          ),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('Monumento',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.amber, fontFamily: GoogleFonts.averageSans(fontWeight: FontWeight.bold).fontFamily, fontSize: 40.0, fontWeight: FontWeight.bold),
                    ),
                    Text('Want to learn more about monuments? Wondering what is that building right infront of you? You are in the right place!',
                    style: TextStyle(color: Colors.orange,fontSize: 20.0, fontFamily: GoogleFonts.averageSans().fontFamily,
                    ),
                    textAlign: TextAlign.center,
                    )
                  ],
                ),
                RaisedButton(onPressed: (){
                  Navigator.pushNamed(context, '/login-screen');
                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                color: Colors.amber,
                padding: EdgeInsets.all(10.0),
                child: Text("Let's Explore Monuments!", style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontFamily: GoogleFonts.averageSans().fontFamily
                  )),
                )
              ],
            ),
          ),
        ],
      ),)
    );
  }
}

