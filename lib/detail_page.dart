import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

import 'models/activity_model.dart';
import 'models/destination.dart';

class DestinationScreen extends StatefulWidget {
  final PopularMonuments popularmonuments;

  DestinationScreen({this.popularmonuments});

  @override
  _DestinationScreenState createState() => _DestinationScreenState();
}

class _DestinationScreenState extends State<DestinationScreen> {
  Text _buildRatingStars(int rating) {
    String stars = '';
    for (int i = 0; i < rating; i++) {
      stars += '⭐ ';
    }
    stars.trim();
    return Text(stars);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0.0, 2.0),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: Hero(
                  tag: widget.popularmonuments.imageUrl,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Image(
                      image: AssetImage(widget.popularmonuments.imageUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      iconSize: 30.0,
                      color: Colors.white,
                      onPressed: () => Navigator.pop(context),
                    ),
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.search),
                          iconSize: 30.0,
                          color: Colors.white,
                          onPressed: () => Navigator.pop(context),
                        ),
                        IconButton(
                          icon: Icon(FontAwesomeIcons.sortAmountDown),
                          iconSize: 25.0,
                          color: Colors.white,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                left: 20.0,
                bottom: 20.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.popularmonuments.city,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35.0,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1.2,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.locationArrow,
                          size: 15.0,
                          color: Colors.white70,
                        ),
                        SizedBox(width: 5.0),
                        Text(
                          widget.popularmonuments.country,
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 20.0,
                          ),
                        ),
                        _buildRatingStars(5)
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 20.0,
                bottom: 20.0,
                child: Icon(
                  FontAwesomeIcons.locationArrow,
                  color: Colors.white70,
                  size: 25.0,
                ),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
                          child: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Wikipedia Info:', style: TextStyle(fontSize: 24.0, fontFamily: GoogleFonts.averageSans().fontFamily,fontWeight: FontWeight.bold)),
                    ),
                    Text(
                      "The Eiffel Tower (/ˈaɪfəl/ EYE-fəl; French: tour Eiffel [tuʁ‿ɛfɛl] (About this soundlisten)) is a wrought-iron lattice tower on the Champ de Mars in Paris, France. It is named after the engineer Gustave Eiffel, whose company designed and built the tower.Constructed from 1887 to 1889 as the entrance to the 1889 World's Fair, it was initially criticised by some of France's leading artists and intellectuals for its design, but it has become a global cultural icon of France and one of the most recognisable structures in the world.[3] The Eiffel Tower is the most-visited paid monument in the world; 6.91 million people ascended it in 2015. The tower is 324 metres (1,063 ft) tall, about the same height as an 81-storey building, and the tallest structure in Paris. Its base is square, measuring 125 metres (410 ft) on each side. During its construction, the Eiffel Tower surpassed the Washington Monument to become the tallest man-made structure in the world, a title it held for 41 years until the Chrysler Building in New York City was finished in 1930. It was the first structure to reach a height of 300 metres. Due to the addition of a broadcasting aerial at the top of the tower in 1957, it is now taller than the Chrysler Building by 5.2 metres (17 ft). Excluding transmitters, the Eiffel Tower is the second tallest free-standing structure in France after the Millau Viaduct. ",
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
            // child: ListView.builder(
            //   padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
            //   itemCount: widget.popularmonuments.activities.length,
            //   itemBuilder: (BuildContext context, int index) {
            //     Activity activity = widget.popularmonuments.activities[index];
            //     return Stack(
            //       children: <Widget>[
            //         Container(
            //           margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
            //           height: 170.0,
            //           width: double.infinity,
            //           decoration: BoxDecoration(
            //             color: Colors.white,
            //             borderRadius: BorderRadius.circular(20.0),
            //           ),
            //           child: Padding(
            //             padding: EdgeInsets.fromLTRB(100.0, 20.0, 20.0, 20.0),
            //             child: Column(
            //               mainAxisAlignment: MainAxisAlignment.center,
            //               crossAxisAlignment: CrossAxisAlignment.start,
            //               children: <Widget>[
            //                 Row(
            //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                   crossAxisAlignment: CrossAxisAlignment.start,
            //                   children: <Widget>[
            //                     Container(
            //                       width: 120.0,
            //                       child: Text(
            //                         activity.name,
            //                         style: TextStyle(
            //                           fontSize: 18.0,
            //                           fontWeight: FontWeight.w600,
            //                         ),
            //                         overflow: TextOverflow.ellipsis,
            //                         maxLines: 2,
            //                       ),
            //                     ),
            //                     Column(
            //                       children: <Widget>[
            //                         Text(
            //                           '\$${activity.price}',
            //                           style: TextStyle(
            //                             fontSize: 22.0,
            //                             fontWeight: FontWeight.w600,
            //                           ),
            //                         ),
            //                         Text(
            //                           'per pax',
            //                           style: TextStyle(
            //                             color: Colors.grey,
            //                           ),
            //                         ),
            //                       ],
            //                     ),
            //                   ],
            //                 ),
            //                 Text(
            //                   activity.type,
            //                   style: TextStyle(
            //                     color: Colors.grey,
            //                   ),
            //                 ),
            //                 _buildRatingStars(activity.rating),
            //                 SizedBox(height: 10.0),
            //                 Row(
            //                   children: <Widget>[
            //                     Container(
            //                       padding: EdgeInsets.all(5.0),
            //                       width: 70.0,
            //                       decoration: BoxDecoration(
            //                         color: Theme.of(context).accentColor,
            //                         borderRadius: BorderRadius.circular(10.0),
            //                       ),
            //                       alignment: Alignment.center,
            //                       child: Text(
            //                         activity.startTimes[0],
            //                       ),
            //                     ),
            //                     SizedBox(width: 10.0),
            //                     Container(
            //                       padding: EdgeInsets.all(5.0),
            //                       width: 70.0,
            //                       decoration: BoxDecoration(
            //                         color: Theme.of(context).accentColor,
            //                         borderRadius: BorderRadius.circular(10.0),
            //                       ),
            //                       alignment: Alignment.center,
            //                       child: Text(
            //                         activity.startTimes[1],
            //                       ),
            //                     ),
            //                   ],
            //                 )
            //               ],
            //             ),
            //           ),
            //         ),
            //         Positioned(
            //           left: 20.0,
            //           top: 15.0,
            //           bottom: 15.0,
            //           child: ClipRRect(
            //             borderRadius: BorderRadius.circular(20.0),
            //             child: Image(
            //               width: 110.0,
            //               image: AssetImage(
            //                 activity.imageUrl,
            //               ),
            //               fit: BoxFit.cover,
            //             ),
            //           ),
            //         ),
            //       ],
            //     );
            //   },
            // ),
          ),
        ],
      ),
    );
  }
}