import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:monumento/models/monuments.dart';
import 'package:monumento/object_detection.dart';
import 'package:monumento/profile_page.dart';

import 'utils/carousel.dart';
import 'utils/carousel_dest.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  int _currentTab = 0;
  List<IconData> _icons = [
    FontAwesomeIcons.plane,
    FontAwesomeIcons.bed,
    FontAwesomeIcons.walking,
    FontAwesomeIcons.biking,
  ];

  Widget _buildIcon(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
      },
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          color: _selectedIndex == index
              ? Colors.amber
              : Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Icon(
          _icons[index],
          size: 25.0,
          color: _selectedIndex == index
              ? Colors.white
              : Colors.amber
        ),
      ),
    );
  }

Future<List> search(String search) async {
  await Future.delayed(Duration(seconds: 2));
  return List.generate(search.length, (int index) {
    return Monument(
      name: search
    );
  });
}
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: GoogleFonts.averageSans().fontFamily),
          home: Scaffold(
        body: _currentTab >=3?UserProfilePage():SafeArea(
          child: Stack(
            children: <Widget>[
              ListView(
                padding: EdgeInsets.symmetric(vertical: 30.0),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, right: 120.0),
                    child: Text(
                      'Monumento',
                      style: TextStyle(
                        fontSize: 30.0,
                        color: Colors.amber,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    height: 80.0,
                    padding: EdgeInsets.only(left: 20.0, right:20.0),
                    child: SearchBar(onSearch: search, onItemFound: null)),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //   children: _icons
                  //       .asMap()
                  //       .entries
                  //       .map(
                  //         (MapEntry map) => _buildIcon(map.key),
                  //       )
                  //       .toList(),
                  // ),
                  SizedBox(height: 20.0),
                  PopularMonumentsCarousel(),
                  SizedBox(height: 20.0),
                  MonumentCarousel(),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>ObjectDetectionPage()));
                },
                backgroundColor: Colors.amber,
                child: Icon(FontAwesomeIcons.building, color: Colors.white),
                )),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentTab,
          onTap: (int value) {
            setState(() {
              _currentTab = value;
              // if(_currentTab >= 3){
              //   Navigator.push(context, MaterialPageRoute(builder: (context)=>UserProfilePage()));
              // }
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 30.0,
                color: Colors.grey,
              ),
              title: SizedBox.shrink(),
              activeIcon: Icon(
                Icons.home,
                size: 30.0,
                color: Colors.amber,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.apps,
                size: 30.0,
                color: Colors.grey,
              ),
              title: SizedBox.shrink(),
              activeIcon: Icon(
                Icons.apps,
                size: 30.0,
                color: Colors.amber,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.bookmark, size: 30.0, color: Colors.grey,),
              title: SizedBox.shrink(),
              activeIcon: Icon(Icons.bookmark, size: 30.0, color: Colors.amber,),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outline, size: 30.0,color: Colors.grey,),
              title: SizedBox.shrink(),
              activeIcon: Icon(Icons.person_outline, size: 30.0,color: Colors.amber,),
            ),
          ],
        ),
      ),
    );
  }
}