import 'activity_model.dart';

class PopularMonuments {
  String imageUrl;
  String city;
  String country;
  String description;
  List<Activity> activities;

  PopularMonuments({
    this.imageUrl,
    this.city,
    this.country,
    this.description,
    this.activities,
  });
}

List<Activity> activities = [
  Activity(
    imageUrl: 'assets/eiffel_tower.jpg',
    name: 'Eiffel Tower',
    type: 'Sightseeing Tour',
    startTimes: ['9:00 am', '11:00 am'],
    rating: 5,
    price: 30,
  ),
  Activity(
    imageUrl: 'assets/taj_mahal.jpeg',
    name: 'Walking Tour and Gonadola Ride',
    type: 'Sightseeing Tour',
    startTimes: ['11:00 pm', '1:00 pm'],
    rating: 4,
    price: 210,
  ),
  Activity(
    imageUrl: 'assets/qutub_minar.jpg',
    name: 'Murano and Burano Tour',
    type: 'Sightseeing Tour',
    startTimes: ['12:30 pm', '2:00 pm'],
    rating: 3,
    price: 125,
  ),
];

List<PopularMonuments> destinations = [
  PopularMonuments(
    imageUrl: 'assets/eiffel_tower.jpg',
    city: 'Eiffel Tower',
    country: 'Italy',
    description: 'Visit Italy for an amazing and unforgettable adventure.',
    activities: activities,
  ),
  PopularMonuments(
    imageUrl: 'assets/taj_mahal.jpeg',
    city: 'Taj Mahal',
    country: 'India',
    description: 'Visit New Delhi for an amazing and unforgettable adventure.',
    activities: activities,
  ),
  PopularMonuments(
    imageUrl: 'assets/qutub_minar.jpg',
    city: 'Qutub Minar',
    country: 'India',
    description: 'Visit India for an amazing and unforgettable adventure.',
    activities: activities,
  ),
  PopularMonuments(
    imageUrl: 'assets/eiffel_tower.jpg',
    city: 'Eiffel Tower',
    country: 'Italy',
    description: 'Visit Italy for an amazing and unforgettable adventure.',
    activities: activities,
  ),
  PopularMonuments(
    imageUrl: 'assets/taj_mahal.jpeg',
    city: 'Taj Mahal',
    country: 'India',
    description: 'Visit New Delhi for an amazing and unforgettable adventure.',
    activities: activities,
  ),
];