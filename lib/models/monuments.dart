class Monument {
  String imageUrl;
  String name;
  String address;
  int price;

  Monument({
    this.imageUrl,
    this.name,
    this.address,
    this.price,
  });
}

final List<Monument> monuments = [
  Monument(
    imageUrl: 'assets/eiffel_tower.jpg',
    name: 'Eiffel Tower',
    address: 'Italy',
    price: 175,
  ),
   Monument(
    imageUrl: 'assets/taj_mahal.jpeg',
    name: 'Qutub Minar',
    address: 'Agra',
    price: 240,
  ),
  Monument(
    imageUrl: 'assets/qutub_minar.jpg',
    name: 'New Delhi',
    address: '',
    price: 300,
  ),
];